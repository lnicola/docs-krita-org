# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-05 03:40+0200\n"
"PO-Revision-Date: 2019-07-09 17:13+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: muisrechts"

#: ../../reference_manual/dockers/overview.rst:1
msgid "Overview of the overview docker."
msgstr "Overzicht van de vastzetter Overzicht."

#: ../../reference_manual/dockers/overview.rst:11
#: ../../reference_manual/dockers/overview.rst:16
msgid "Overview"
msgstr "Overzicht"

#: ../../reference_manual/dockers/overview.rst:11
msgid "Navigation"
msgstr "Navigatie"

#: ../../reference_manual/dockers/overview.rst:19
msgid ".. image:: images/dockers/Krita_Overview_Docker.png"
msgstr ".. image:: images/dockers/Krita_Overview_Docker.png"

#: ../../reference_manual/dockers/overview.rst:20
msgid ""
"This docker allows you to see a full overview of your image. You can also "
"use it to navigate and zoom in and out quickly. Dragging the view-rectangle "
"allows you quickly move the view."
msgstr ""
"Deze vastzetter biedt u het zien van een volledig overzicht van uw "
"afbeelding. U kunt het ook gebruiken om erin te navigeren en snel in en uit "
"te zoomen. Slepen met de weergaverechthoek biedt u snel de weergave te "
"verplaatsen."

#: ../../reference_manual/dockers/overview.rst:22
msgid ""
"There are furthermore basic navigation functions: Dragging the zoom-slider "
"allows you quickly change the zoom."
msgstr ""
"Er zijn  verder basis navigatiefuncties: slepen op the zoomschuifregelaar "
"biedt u snel de zoom te wijzigen."

#: ../../reference_manual/dockers/overview.rst:26
msgid ""
"Toggling the mirror button will allow you to mirror the view of the canvas "
"(but not the full image itself) and dragging the rotate slider allows you to "
"adjust the rotation of the viewport. To reset the rotation, |mouseright| the "
"slider to edit the number, and type '0'."
msgstr ""
"Omschakelen met de knop spiegelen laat u de weergave van het werkveld (maar "
"niet de volledige afbeelding zelf) spiegelen en slepen met de draairegelaar "
"biedt u het aanpassen van de rotatie van het weergavegedeelte. Om de rotatie "
"te herstellen, |muisrechts| de regelaar om het getal te bewerken, en typ '0'."
