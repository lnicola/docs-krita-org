# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:23+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.3\n"

#: ../../reference_manual/layers_and_masks/paint_layers.rst:1
msgid "How to use paint layers in Krita."
msgstr "Hoe tekenlagen in Krita gebruiken."

#: ../../reference_manual/layers_and_masks/paint_layers.rst:15
msgid "Layers"
msgstr "Lagen"

#: ../../reference_manual/layers_and_masks/paint_layers.rst:15
msgid "Paint Layer"
msgstr "Tekenlaag"

#: ../../reference_manual/layers_and_masks/paint_layers.rst:15
msgid "Raster"
msgstr "Raster"

#: ../../reference_manual/layers_and_masks/paint_layers.rst:15
msgid "Bitmap"
msgstr "Bitmap"

#: ../../reference_manual/layers_and_masks/paint_layers.rst:20
msgid "Paint Layers"
msgstr "Tekenlagen"

#: ../../reference_manual/layers_and_masks/paint_layers.rst:22
msgid ""
"Paint layers are the most commonly used type of layers used in digital paint "
"or image manipulation software like Krita. If you've ever used layers in :"
"program:`Photoshop` or the :program:`Gimp`, you'll be used to how they work. "
"In short, a paint layer, also called a pixel, bitmap or raster layer, is a "
"bitmap image (an image made up of many points of color)."
msgstr ""

#: ../../reference_manual/layers_and_masks/paint_layers.rst:24
msgid ""
"Paint layers let you apply many advanced effects such as smearing, smudging "
"and distorting. This makes them the most flexible type of layer. However,  "
"paint layers don't scale well when enlarged (they pixelate), and any effects "
"that have been applied can't be edited."
msgstr ""

#: ../../reference_manual/layers_and_masks/paint_layers.rst:26
msgid ""
"To deal with these two drawbacks, digital artists will typically work at "
"higher Pixel Per Inch (PPI) counts.  It is not unusual to see PPI settings "
"of 400 to 600 PPI for a canvas with a good amount of detail.  To combat the "
"issue of applied effects that cannot be edited it is best to take advantage "
"of the non-destructive layer capabilities of filter, transparency and "
"transform masks."
msgstr ""

#: ../../reference_manual/layers_and_masks/paint_layers.rst:28
msgid ""
"As long as you have enough resolution / size on your canvas though, and as "
"long as you aren't going to need to go back and tweak an effect you created "
"previously, then a paint layer is usually the type of layer you will want. "
"If you click on the :guilabel:`New layer` icon in the layers docker you'll "
"get a paint layer. Of course you can always choose the :guilabel:`New layer` "
"drop-down to get another type."
msgstr ""

#: ../../reference_manual/layers_and_masks/paint_layers.rst:30
msgid "The hotkey for adding a new paint layer is the :kbd:`Ins` key."
msgstr ""
"De sneltoets voor een nieuwe tekenlaag toevoegen is de toets :kbd:`Ins`."
