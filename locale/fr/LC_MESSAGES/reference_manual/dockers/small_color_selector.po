msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-27 01:41+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../reference_manual/dockers/small_color_selector.rst:1
msgid "Overview of the small color selector docker."
msgstr ""

#: ../../reference_manual/dockers/small_color_selector.rst:11
#: ../../reference_manual/dockers/small_color_selector.rst:16
msgid "Small Color Selector"
msgstr "Sélecteur de couleurs simplifié"

#: ../../reference_manual/dockers/small_color_selector.rst:11
msgid "Color"
msgstr ""

#: ../../reference_manual/dockers/small_color_selector.rst:11
#, fuzzy
#| msgid "Small Color Selector"
msgid "Color Selector"
msgstr "Sélecteur de couleurs simplifié"

#: ../../reference_manual/dockers/small_color_selector.rst:19
msgid ".. image:: images/dockers/Krita_Small_Color_Selector_Docker.png"
msgstr ""

#: ../../reference_manual/dockers/small_color_selector.rst:20
msgid ""
"This is Krita's most simple color selector. On the left there's a bar with "
"the hue, and on the right a square where you can pick the value and "
"saturation."
msgstr ""

#: ../../reference_manual/dockers/small_color_selector.rst:24
msgid ""
"The small color selector is the only selector which can show HDR values. "
"When your build of Krita is HDR enabled and you are on Windows, you can drag "
"the slider at the bottom to increase the 'nits' of the colors in the small "
"selector. This is the direct value of the brightness of the colors, and you "
"need a value above 100 (100 being the maximum value used for the brightest "
"value of sRGB colors), to have an HDR color. The small color selector will "
"also select wide gamut values."
msgstr ""
