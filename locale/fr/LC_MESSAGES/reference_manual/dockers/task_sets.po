msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-27 02:38+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../reference_manual/dockers/task_sets.rst:1
msgid "Overview of the task sets docker."
msgstr ""

#: ../../reference_manual/dockers/task_sets.rst:16
msgid "Task Sets Docker"
msgstr ""

#: ../../reference_manual/dockers/task_sets.rst:18
msgid ""
"Task sets are for sharing a set of steps, like a tutorial. You make them "
"with the task-set docker."
msgstr ""

#: ../../reference_manual/dockers/task_sets.rst:21
msgid ".. image:: images/dockers/Task-set.png"
msgstr ""

#: ../../reference_manual/dockers/task_sets.rst:22
msgid ""
"Task sets can record any kind of command also available via the shortcut "
"manager. It can not record strokes, like the macro recorder can. However, "
"you can play macros with the tasksets!"
msgstr ""

#: ../../reference_manual/dockers/task_sets.rst:24
msgid ""
"The tasksets docker has a record button, and you can use this to record a "
"certain workflow. Then use this to let items appear in the taskset list. "
"Afterwards, turn off the record. You can then click any action in the list "
"to make them happen. Press the 'Save' icon to name and save the taskset."
msgstr ""
