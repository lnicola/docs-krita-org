msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-02-27 01:00+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../general_concepts/file_formats/file_kra.rst:1
msgid "The Krita Raster Archive file format."
msgstr ""

#: ../../general_concepts/file_formats/file_kra.rst:10
msgid "*.kra"
msgstr ""

#: ../../general_concepts/file_formats/file_kra.rst:10
msgid "KRA"
msgstr ""

#: ../../general_concepts/file_formats/file_kra.rst:10
msgid "Krita Archive"
msgstr ""

#: ../../general_concepts/file_formats/file_kra.rst:15
msgid "\\*.kra"
msgstr ""

#: ../../general_concepts/file_formats/file_kra.rst:17
msgid ""
"``.kra`` is Krita's internal file-format, which means that it is the file "
"format that saves all of the features Krita can handle. It's construction is "
"vaguely based on the open document standard, which means that you can rename "
"your ``.kra`` file to a ``.zip`` file and open it up to look at the insides."
msgstr ""

#: ../../general_concepts/file_formats/file_kra.rst:19
msgid ""
"It is a format that you can expect to get very heavy, and isn't meant for "
"sharing on the internet."
msgstr ""
