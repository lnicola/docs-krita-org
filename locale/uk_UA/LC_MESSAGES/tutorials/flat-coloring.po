# Translation of docs_krita_org_tutorials___flat-coloring.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_tutorials___flat-coloring\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-09-02 03:25+0200\n"
"PO-Revision-Date: 2019-08-02 11:40+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart14.png\n"
"   :alt: layer structure for flatting in krita"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart14.png\n"
"   :alt: Структура шарів для спрощення у krita"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart1.png\n"
"   :alt: blend mode setup of line art flat coloring"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart1.png\n"
"   :alt: Налаштовування режиму змішування для розфарбовування графіки"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart2.png\n"
"   :alt: effects of multiply blend mode"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart2.png\n"
"   :alt: ефекти режиму змішування множенням"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/icons/fill_tool.svg\n"
"   :alt: fill-tool icon"
msgstr ""
".. image:: images/icons/fill_tool.svg\n"
"   :alt: Піктограма інструмента заповнення"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart7.png\n"
"   :alt: colors filled with fill tool"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart7.png\n"
"   :alt: Заповнення кольором за допомогою інструмента заповнення"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart15.png\n"
"   :alt: selecting with selection tools for filling color"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart15.png\n"
"   :alt: Позначення за допомогою інструментів позначення для заповнення "
"кольором"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart16.png\n"
"   :alt: selection mask in Krita"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart16.png\n"
"   :alt: Маска позначення у Krita"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart17.png\n"
"   :alt: filling color in selection"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart17.png\n"
"   :alt: Заповнення кольором у позначеній області"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart18.png\n"
"   :alt: result of coloring made with the help of selection tools"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart18.png\n"
"   :alt: Результат розфарбовування за допомогою інструментів позначення"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart8.png\n"
"   :alt: filling color in line art using path tool"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart8.png\n"
"   :alt: Заповнення кольором графіки за допомогою інструмента контурів"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart9.png\n"
"   :alt: erasing with path tool"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart9.png\n"
"   :alt: Витирання за допомогою інструмента контуру"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart10.png\n"
"   :alt: coloring with colorize mask"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart10.png\n"
"   :alt: Зафарбовування за допомогою маски розфарбовування"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart11.png\n"
"   :alt: result from the colorize mask"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart11.png\n"
"   :alt: Результат застосування маски розфарбовування"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart12.png\n"
"   :alt: slitting colors into islands"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart12.png\n"
"   :alt: Розрізання кольорів на острови"

#: ../../tutorials/flat-coloring.rst:None
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart13.png\n"
"   :alt: resulting color islands from split layers"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart13.png\n"
"   :alt: Кольорові острови — результати поділу шарів"

#: ../../tutorials/flat-coloring.rst:0
msgid ".. image:: images/flat-coloring/Krita_filling_lineart_selection_1.png"
msgstr ".. image:: images/flat-coloring/Krita_filling_lineart_selection_1.png"

#: ../../tutorials/flat-coloring.rst:0
msgid ".. image:: images/flat-coloring/Krita_filling_lineart_selection_2.png"
msgstr ".. image:: images/flat-coloring/Krita_filling_lineart_selection_2.png"

#: ../../tutorials/flat-coloring.rst:0
msgid ".. image:: images/flat-coloring/Krita_filling_lineart_mask_1.png"
msgstr ".. image:: images/flat-coloring/Krita_filling_lineart_mask_1.png"

#: ../../tutorials/flat-coloring.rst:0
msgid ".. image:: images/flat-coloring/Krita_filling_lineart_mask_2.png"
msgstr ".. image:: images/flat-coloring/Krita_filling_lineart_mask_2.png"

#: ../../tutorials/flat-coloring.rst:0
msgid ".. image:: images/flat-coloring/Krita_filling_lineart_mask_3.png"
msgstr ".. image:: images/flat-coloring/Krita_filling_lineart_mask_3.png"

#: ../../tutorials/flat-coloring.rst:1
msgid "Common workflows used in Krita"
msgstr "Типові робочі процедури у Krita"

#: ../../tutorials/flat-coloring.rst:13
msgid "Flat Coloring"
msgstr "Однотонне зафарбовування"

#: ../../tutorials/flat-coloring.rst:15
msgid ""
"So you've got a cool black on white drawing, and now you want to color it! "
"The thing we’ll aim for in this tutorial is to get your line art colored in "
"with flat colors. So no shading just yet. We’ll be going through some "
"techniques for preparing the line art, and we’ll be using the layer docker "
"to put each color on a separate layer, so we can easily access each color "
"when we add shading."
msgstr ""
"Отже, нехай у вас є чудовий чорно-білий малюнок і ви хочете розфарбувати "
"його! Питання, яке ми розглянемо у цьому розділі підручника, пов'язане із "
"розфарбовуванням графіки простими кольорами. Отже, питання малювання тіней "
"не розглядатимемо. Ми ознайомимося із декількома методиками приготування "
"графіки, потім скористаємося бічною панеллю шарів для розташовування кожного "
"з кольорів у окремому шарі, щоб полегшити до нього доступ і, згодом, додати "
"тіні."

#: ../../tutorials/flat-coloring.rst:17
msgid ""
"This tutorial is adapted from this `tutorial <http://theratutorial.tumblr."
"com/post/66584924501/flat-colouring-in-the-kingdom-of-2d-layers-are>`_ by "
"the original author."
msgstr ""
"Цей розділ підручника написано на основі `цього підручника <http://"
"theratutorial.tumblr.com/post/66584924501/flat-colouring-in-the-kingdom-"
"of-2d-layers-are>`_."

#: ../../tutorials/flat-coloring.rst:20
msgid "Understanding Layers"
msgstr "Поняття шарів"

#: ../../tutorials/flat-coloring.rst:22
msgid ""
"To fill line art comfortably, it's best to take advantage of the layerstack. "
"The layer stack is pretty awesome, and it's one of those features that make "
"digital art super-convenient."
msgstr ""
"Для зручного розмальовування графіки варто скористатися перевагами стосу "
"шарів. Стос шарів є чудовим винаходом — це одна з можливостей, які роблять "
"цифрове малювання надзвичайно зручним."

#: ../../tutorials/flat-coloring.rst:24
msgid ""
"In traditional art, it is not uncommon to first draw the full background "
"before drawing the subject. Or to first draw a line art and then color it "
"in. Computers have a similar way of working."
msgstr ""
"У традиційному малюванні доволі поширеними є методики малювання усього тла "
"до малювання об'єкта картини. Також часто малюють графіку, а потім її "
"розфарбовують. На комп'ютерах використовують подібні ж підходи до побудови "
"картин."

#: ../../tutorials/flat-coloring.rst:26
msgid ""
"In programming, if you tell a computer to draw a red circle, and then "
"afterwards tell it to draw a smaller yellow circle, you will see the small "
"yellow circle overlap the red circle. Switch the commands around, and you "
"will not see the yellow circle at all: it was drawn before the red circle "
"and thus ‘behind’ it."
msgstr ""
"У програмуванні якщо ви накажете комп'ютеру намалювати червоне коло, а потім "
"намалювати на ньому менше жовте коло, результатом буде перекриття жовтим "
"колом червоного. Поміняйте команди малювання місцями, і ви не побачите "
"жовтого кола взагалі: його було намальовано до червоного кола, а отже, воно "
"опиниться «під» червоним колом."

#: ../../tutorials/flat-coloring.rst:28
msgid ""
"This is referred to as the “drawing order”. So like the traditional artist, "
"the computer will first draw the images that are behind everything, and "
"layer the subject and foreground on top of it. The layer docker is a way for "
"you to control the drawing order of multiple images, so for example, you can "
"have your line art drawn later than your colors, meaning that the lines will "
"be drawn over the colors, making it easier to make it neat!"
msgstr ""
"Результат визначається тим, що ми називаємо «порядок малювання». Отже, "
"подібно до традиційного художника, комп'ютер спершу малює зображення, які "
"розташовуються на задньому плані, і накладає об'єкт і передній план на "
"нього. За допомогою бічної панелі шарів ви можете керувати порядком "
"малювання декількох зображень, зокрема, наприклад, ви можете намалювати "
"графіку пізніше за кольори, тобто лінії буде намальовано над кольорами, що "
"полегшує створення акуратних малюнків!"

#: ../../tutorials/flat-coloring.rst:30
msgid ""
"Other things that a layer stack can do are blending the colors of different "
"layers differently with blending modes, using a filter in the layer stack, "
"or using a mask that allows you to make parts transparent."
msgstr ""
"Серед інших речей, які можна робити за допомогою стосу шарів, є змішування "
"кольорів різних шарів по-різному за допомогою режимів змішування, "
"використання фільтра у стосі шарів та використання маски, яка уможливлює "
"перетворення частин зображення на прозорі."

#: ../../tutorials/flat-coloring.rst:32
msgid ""
"Programmers talk about transparency as ''Alpha'', which is because the 'a' "
"symbol is used to present transparency in the algorithms for painting one "
"color on top of another. Usually when you see the word ''Alpha'' in a "
"graphics program, just think of it as affecting the transparency."
msgstr ""
"Програмісти називають прозорість «альфою», оскільки символ «α» "
"використовується для позначення прозорості у алгоритмах для малювання одного "
"кольору над іншим. Зазвичай, якщо ви бачите слово «альфа» у графічній "
"програмі, це щось, що впливає на прозорість."

#: ../../tutorials/flat-coloring.rst:35
msgid "Preparing your line art"
msgstr "Приготування вашої графіки"

#: ../../tutorials/flat-coloring.rst:37
msgid ""
"Put the new layer underneath the layer containing the line art (drag and "
"drop or use the up/down arrows for that), and draw on it."
msgstr ""
"Розташуйте новий шар під шаром графіки (перетягуванням зі скиданням або за "
"допомогою кнопок зі стрілками вгору-вниз) і малюйте на ньому."

#: ../../tutorials/flat-coloring.rst:42
msgid ""
"…And notice nothing happening. This is because the white isn’t transparent. "
"You wouldn’t really want it to either, how else would you make convincing "
"highlights? So what we first need to do to color in our drawing is prepare "
"our line art. There’s several methods of doing so, each with varying "
"qualities."
msgstr ""
"…І нічого не станеться. Причиною є те, що білий колір не є прозорим. І "
"навряд чи нам потрібно робити його прозорим. Як же інакше можна створити "
"переконливі виблиски на зображенні? Для розфарбовування нашого зображення "
"нам спершу доведеться приготувати графіку. Існує декілька способів зробити "
"це, і у кожного з цих способів є власні особливості."

#: ../../tutorials/flat-coloring.rst:45
msgid "The Multiply Blending Mode"
msgstr "Режим змішування «Множення»"

#: ../../tutorials/flat-coloring.rst:47
msgid ""
"So, typically, to get a black and white line art usable for coloring, you "
"can set the blending mode of the line art layer to Multiply. You do this by "
"selecting the layer and going to the drop-down that says **Normal** and "
"setting that to **Multiply**."
msgstr ""
"Отже, типово, для того, щоб чорно-біла графіка стала придатною для "
"розфарбовування, вам слід встановити режим змішування для шару графіки у "
"значення «Множення». Для цього позначте шар у списку шарів і виберіть у "
"спадному списку, не типовим значенням є **Звичайний** пункт **Множення**."

#: ../../tutorials/flat-coloring.rst:52
msgid "And then you should be able to see your colors!"
msgstr "І тепер ви маєте побачити ваші кольори!"

#: ../../tutorials/flat-coloring.rst:54
msgid ""
"Multiply is not a perfect solution however. For example, if through some "
"image editing magic I make the line art blue, it results into this:"
msgstr ""
"Втім, множення не є ідеальним рішенням. Наприклад, якщо за допомогою певних "
"трюків із редагування зображення вдасться зробити графіку синьою, результат "
"виглядатиме так:"

#: ../../tutorials/flat-coloring.rst:59
msgid ""
"This is because multiply literally multiplies the colors. So it uses maths!"
msgstr ""
"Причиною є те, що множення буквально множить кольори між собою. Отже, маємо "
"справу із математикою!"

#: ../../tutorials/flat-coloring.rst:61
msgid ""
"What it first does is take the values of the RGB channels, then divides them "
"by the max (because we're in 8bit, this is 255), a process we call "
"normalising. Then it multiplies the normalized values. Finally, it takes the "
"result and multiplies it with 255 again to get the result values."
msgstr ""
"У цьому режимі програма спочатку визначає значення каналів RGB, потім ділить "
"їх на максимальне значення (оскільки маємо справу із 8-бітовими каналами, це "
"значення дорівнює 255), тобто виконує процедуру, яка називається "
"нормалізацією. Потім програма множить нормалізовані значення. Нарешті, "
"програма множить результат на 255 знову, щоб отримати остаточний результат."

#: ../../tutorials/flat-coloring.rst:67
msgid "Pink"
msgstr "Рожевий"

#: ../../tutorials/flat-coloring.rst:68
msgid "Pink (normalized)"
msgstr "Рожевий (нормалізований)"

#: ../../tutorials/flat-coloring.rst:69 ../../tutorials/flat-coloring.rst:87
msgid "Blue"
msgstr "Синій"

#: ../../tutorials/flat-coloring.rst:70
msgid "Blue (normalized)"
msgstr "Синій (нормалізований)"

#: ../../tutorials/flat-coloring.rst:71
msgid "Normalized, multiplied"
msgstr "Нормалізовано, множення"

#: ../../tutorials/flat-coloring.rst:72
msgid "Result"
msgstr "Результат"

#: ../../tutorials/flat-coloring.rst:73
msgid "Red"
msgstr "Червоний"

#: ../../tutorials/flat-coloring.rst:74
msgid "222"
msgstr "222"

#: ../../tutorials/flat-coloring.rst:75
msgid "0.8705"
msgstr "0.8705"

#: ../../tutorials/flat-coloring.rst:76
msgid "92"
msgstr "92"

#: ../../tutorials/flat-coloring.rst:77
msgid "0.3607"
msgstr "0.3607"

#: ../../tutorials/flat-coloring.rst:78
msgid "0.3139"
msgstr "0.3139"

#: ../../tutorials/flat-coloring.rst:79
msgid "80"
msgstr "80"

#: ../../tutorials/flat-coloring.rst:80
msgid "Green"
msgstr "Зелений"

#: ../../tutorials/flat-coloring.rst:81
msgid "144"
msgstr "144"

#: ../../tutorials/flat-coloring.rst:82
msgid "0.5647"
msgstr "0.5647"

#: ../../tutorials/flat-coloring.rst:83
msgid "176"
msgstr "176"

#: ../../tutorials/flat-coloring.rst:84
msgid "0.6902"
msgstr "0.6902"

#: ../../tutorials/flat-coloring.rst:85
msgid "0.3897"
msgstr "0.3897"

#: ../../tutorials/flat-coloring.rst:86
msgid "99"
msgstr "99"

#: ../../tutorials/flat-coloring.rst:88
msgid "123"
msgstr "123"

#: ../../tutorials/flat-coloring.rst:89
msgid "0.4823"
msgstr "0.4823"

#: ../../tutorials/flat-coloring.rst:90
msgid "215"
msgstr "215"

#: ../../tutorials/flat-coloring.rst:91
msgid "0.8431"
msgstr "0.8431"

#: ../../tutorials/flat-coloring.rst:92
msgid "0.4066"
msgstr "0.4066"

#: ../../tutorials/flat-coloring.rst:93
msgid "103"
msgstr "103"

#: ../../tutorials/flat-coloring.rst:95
msgid ""
"This isn't completely undesirable, and a lot of artists use this effect to "
"add a little richness to their colors."
msgstr ""
"Цей ефект не є повністю небажаним. Багато художників використовують його для "
"того, щоб дещо збагатити кольори зображення."

#: ../../tutorials/flat-coloring.rst:98 ../../tutorials/flat-coloring.rst:127
#: ../../tutorials/flat-coloring.rst:154 ../../tutorials/flat-coloring.rst:173
#: ../../tutorials/flat-coloring.rst:210 ../../tutorials/flat-coloring.rst:247
#: ../../tutorials/flat-coloring.rst:275 ../../tutorials/flat-coloring.rst:317
msgid "Advantages"
msgstr "Переваги"

#: ../../tutorials/flat-coloring.rst:100
msgid ""
"Easy, can work to your benefit even with colored lines by softening the look "
"of the lines while keeping nice contrast."
msgstr ""
"Простий, може працювати з користю, навіть із кольоровими лініями, "
"пом'якшуючи вигляд ліній і зберігаючи чудовий контраст."

#: ../../tutorials/flat-coloring.rst:103 ../../tutorials/flat-coloring.rst:132
#: ../../tutorials/flat-coloring.rst:159 ../../tutorials/flat-coloring.rst:178
#: ../../tutorials/flat-coloring.rst:215 ../../tutorials/flat-coloring.rst:252
#: ../../tutorials/flat-coloring.rst:280 ../../tutorials/flat-coloring.rst:322
msgid "Disadvantages"
msgstr "Недоліки"

#: ../../tutorials/flat-coloring.rst:105
msgid "Not actually transparent. Is a little funny with colored lines."
msgstr "Не є насправді прозорим. Не дуже добре працює із кольоровими лініями."

#: ../../tutorials/flat-coloring.rst:108
msgid "Using Selections"
msgstr "Користування позначенням"

#: ../../tutorials/flat-coloring.rst:110
msgid ""
"The second method is one where we'll make it actually transparent. In other "
"programs this would be done via the channel docker, but Krita doesn't do "
"custom channels, instead it uses Selection Masks to store custom selections."
msgstr ""
"Другий метод полягає у перетворенні тла графіки на справді прозоре. У інших "
"програмах, це можна зробити за допомогою бічної панелі каналів, але Krita не "
"працює із нетиповими каналами, замість цього програма використовує маски "
"позначення для зберігання даних нетипових позначених ділянок."

#: ../../tutorials/flat-coloring.rst:112
msgid "Duplicate your line art layer."
msgstr "Здублюйте ваш шар графіки."

#: ../../tutorials/flat-coloring.rst:114
msgid ""
"Convert the duplicate to a selection mask. |mouseright| the layer, then :"
"menuselection:`Convert --> to Selection Mask`."
msgstr ""
"Перетворіть дублікат на маску вибору. |mouseright| на пункті шару, далі :"
"menuselection:`Перетворити --> на маску вибору`."

#: ../../tutorials/flat-coloring.rst:118
msgid ""
"Invert the selection mask. :menuselection:`Select --> Invert Selection`."
msgstr ""
"Інвертуйте маску позначення. :menuselection:`Вибір --> Інвертувати вибір`."

#: ../../tutorials/flat-coloring.rst:120
msgid ""
"Make a new layer, and do :menuselection:`Edit --> Fill with Foreground "
"Color`."
msgstr ""
"Створіть шар і скористайтеся пунктом меню :menuselection:`Зміни --> "
"Заповнити кольором переднього плану`."

#: ../../tutorials/flat-coloring.rst:124
msgid "And you should now have the line art on a separate layer."
msgstr "Тепер графіку має бути розташовано на окремому шарі."

#: ../../tutorials/flat-coloring.rst:129
msgid "Actual transparency."
msgstr "Справжня прозорість"

#: ../../tutorials/flat-coloring.rst:134
msgid "Doesn't work when the line art is colored."
msgstr "Не працює, якщо графіку розфарбовано."

#: ../../tutorials/flat-coloring.rst:137
msgid "Using Masks"
msgstr "Користування масками"

#: ../../tutorials/flat-coloring.rst:139
msgid "This is a simpler variation of the above."
msgstr "Це простіший різновид описаного вище."

#: ../../tutorials/flat-coloring.rst:141
msgid "Make a filled layer underneath the line art layer."
msgstr "Створіть заповнений шар під шаром графіки."

#: ../../tutorials/flat-coloring.rst:145
msgid ""
"Convert the line art layer to a transparency mask |mouseright| the layer, "
"then :menuselection:`Convert --> to Transparency Mask`."
msgstr ""
"Перетворіть шар графіки на маску прозорості: клацніть |mouseright| на пункті "
"шару, виберіть пункт :menuselection:`Перетворити --> на маску прозорості`."

#: ../../tutorials/flat-coloring.rst:149
msgid ""
"Invert the transparency mask by going to :menuselection:`Filter --> Adjust --"
"> Invert`"
msgstr ""
"Оберніть маску прозорості за допомогою пункту меню :menuselection:`Фільтр --"
"> Скоригувати --> Інвертувати`"

#: ../../tutorials/flat-coloring.rst:156
msgid ""
"Actual transparency. You can also very easily doodle a pattern on the filled "
"layer where the mask is on without affecting the transparency."
msgstr ""
"Справжня прозорість. Крім того, ви можете дуже просто намалювати візерунок "
"на заповненому шарі, де зберігається маска, без зміни прозорості."

#: ../../tutorials/flat-coloring.rst:161
msgid ""
"Doesn't work when the line art is colored already. We can still get faster."
msgstr "Не працює, якщо графіку вже розфарбовано. Могло б працювати і швидше."

#: ../../tutorials/flat-coloring.rst:164
msgid "Using Color to Alpha"
msgstr "Перетворення кольору на альфу (канал прозорості)"

#: ../../tutorials/flat-coloring.rst:166
msgid "By far the fastest way to get transparent line art."
msgstr "У поточній версії, найшвидший спосіб створити прозору графіку."

#: ../../tutorials/flat-coloring.rst:168
msgid ""
"Select the line art layer and apply the color to alpha filter. :"
"menuselection:`Filter --> Colors --> Color to Alpha`. The default values "
"should be sufficient for line art."
msgstr ""
"Позначте пункт шару графіки і застосуйте фільтр перетворення кольору на "
"прозорість. :menuselection:`Фільтр --> Колір --> Колір до альфи`. Типові "
"значення мають бути достатніми для графіки."

#: ../../tutorials/flat-coloring.rst:171
msgid ""
".. image:: images/flat-coloring/Krita_filling_lineart_color_to_alpha.png"
msgstr ""
".. image:: images/flat-coloring/Krita_filling_lineart_color_to_alpha.png"

#: ../../tutorials/flat-coloring.rst:175
msgid ""
"Actual transparency. Works with colored line art as well, because it removes "
"the white specifically."
msgstr ""
"Справжня прозорість. Працює із кольоровою графікою, оскільки вилучає лише "
"білий колір."

#: ../../tutorials/flat-coloring.rst:180
msgid ""
"You'll have to lock the layer transparency or separate out the alpha via the "
"right-click menu if you want to easily color it."
msgstr ""
"Вам слід заблокувати прозорість шару і відокремити альфу за допомогою "
"контекстного меню, якщо хочете без проблем розфарбувати його."

#: ../../tutorials/flat-coloring.rst:184
msgid "Coloring the image"
msgstr "Розфарбовування зображення"

#: ../../tutorials/flat-coloring.rst:186
msgid ""
"Much like preparing the line art, there are many different ways of coloring "
"a layer."
msgstr ""
"Дуже подібно до приготування графіки, існує багато різних способів "
"розфарбовування шару."

#: ../../tutorials/flat-coloring.rst:188
msgid ""
"You could for example fill in everything by hand, but while that is very "
"precise it also takes a lot of work. Let's take a look at the other options, "
"shall we?"
msgstr ""
"Ви, наприклад, можете заповнити усе вручну, але хоча це дає надзвичайну "
"точність, це потребує багато зусиль. Може розглянемо інші варіанти?"

#: ../../tutorials/flat-coloring.rst:191
msgid "Fill Tool"
msgstr "Інструмент «Заповнення»"

#: ../../tutorials/flat-coloring.rst:196
msgid ""
"In most cases the fill-tool can’t deal with the anti-aliasing (the soft edge "
"in your line art to make it more smooth when zoomed out) In Krita you have "
"the grow-shrink option. Setting that to say… 2 expands the color two pixels."
msgstr ""
"У більшості випадків інструмент заповнення кольором не може працювати зі "
"згладженими ділянками (пом'якшенням меж графіки з метою зробити графіку "
"плавнішою при зменшенні масштабу). У Krita ви можете скористатися дією "
"розтягування-стискання. Встановімо, наприклад, значення… 2, щоб розтягнути "
"колір на два пікселі."

#: ../../tutorials/flat-coloring.rst:198
msgid ""
"Threshold decides when the fill-tool should consider a different color pixel "
"to be a border. And the feathering adds an extra soft border to the fill."
msgstr ""
"Поріг визначає значення, за якого інструмент заповнення кольором вважатиме "
"колір відмінним від кольору межі. Згладжування додає додаткову м'яку межу "
"ділянці заповнення."

#: ../../tutorials/flat-coloring.rst:200
msgid ""
"Now, if you click on a gapless-part of the image with your preferred color… "
"(Remember to set the opacity to 1.0!)"
msgstr ""
"Тепер, якщо ви клацнете на частині зображення без прогалин, використовуючи "
"пензель і бажаним кольором… (не забудьте встановити непрозорість у значення "
"1.0!)"

#: ../../tutorials/flat-coloring.rst:202
msgid ""
"Depending on your line art, you can do flats pretty quickly. But setting the "
"threshold low can result in little artifacts around where lines meet:"
msgstr ""
"Залежно від вашої графіки, ви зможете створювати заповнені ділянки доволі "
"швидко. Але встановлення низького порогового значення може призвести до "
"певних дефектів у місцях, де з'єднуються лінії:"

#: ../../tutorials/flat-coloring.rst:207
msgid ""
"However, setting the threshold high can end with the fill not recognizing "
"some of the lighter lines. Besides these little artifacts can be removed "
"with the brush easily."
msgstr ""
"Втім, встановлення надто високого порогового значення може призвести до "
"того, що при заповненні не буде враховано деякі світлі лінії. Втім, ці "
"невеличкі дефекти може бути без проблем усунуто за допомогою пензля."

#: ../../tutorials/flat-coloring.rst:212
msgid "Pretty darn quick depending on the available settings."
msgstr "Доволі швидко, залежно від доступних параметрів."

#: ../../tutorials/flat-coloring.rst:217
msgid ""
"Again, not great with gaps or details. And it works best with aliased line "
"art."
msgstr ""
"Знову ж таки, не дуже добре працює із прогалинами або деталями. Найкраще "
"працює із творами із незгладженими лініями."

#: ../../tutorials/flat-coloring.rst:220
msgid "Selections"
msgstr "Позначення"

#: ../../tutorials/flat-coloring.rst:222
msgid "Selections work using the selection tools."
msgstr "Позначення виконується за допомогою інструментів позначення."

#: ../../tutorials/flat-coloring.rst:227
msgid ""
"For example with the :ref:`bezier_curve_selection_tool` you can easily "
"select a curved area, and the with :kbd:`Shift +` |mouseleft| (not |"
"mouseleft| :kbd:`+ Shift`, there's a difference!) you can easily add to an "
"existing selection."
msgstr ""
"Наприклад, за допомогою :ref:`інструмента позначення кривою Безьє "
"<bezier_curve_selection_tool>` ви можете без проблем позначити ділянку із "
"криволінійною межею, а потім за допомогою комбінації  :kbd:`Shift` + |"
"mouseleft| (не |mouseleft| + :kbd:`Shift`, — це різні комбінації!) ви можете "
"без проблем додати нові позначення до наявного позначення."

#: ../../tutorials/flat-coloring.rst:232
msgid ""
"You can also edit the selection if you have :menuselection:`Select --> Show "
"Global Selection Mask` turned on. Then you can select the global selection "
"mask, and paint on it. (Above with the alternative selection mode, activated "
"in the lower-left corner of the stats bar)"
msgstr ""
"Ви також можете редагувати позначену ділянку, якщо позначено пункт меню :"
"menuselection:`Вибір --> Показати маску загального вибору`. Потім ви можете "
"вибрати маску загального вибору і малювати на ній. (Вище за допомогою "
"альтернативного режиму вибору, який можна задіяти у нижньому лівому куті "
"смужки стану.)"

#: ../../tutorials/flat-coloring.rst:234
msgid ""
"When done, select the color you want to fill it with and press the :kbd:"
"`Shift + Backspace` shortcut."
msgstr ""
"Коли справу буде зроблено, виберіть потрібний колір заповнення і натисніть "
"комбінацію клавіш :kbd:`Shift` + `Backspace`."

#: ../../tutorials/flat-coloring.rst:239
msgid ""
"You can save selections in selection masks by |mouseright| a layer, and then "
"going to :menuselection:`Add --> Local Selection`. You first need to "
"deactivate a selection by pressing the circle before adding a new selection."
msgstr ""
"Ви можете зберегти позначені ділянки у масках позначення за допомогою "
"клацання |mouseright| на шарі із наступним вибором пункту меню :"
"menuselection:`Додати --> Локальне позначення`. Спочатку вам слід "
"деактивувати позначене натисканням кола перед додаванням нового позначення."

#: ../../tutorials/flat-coloring.rst:241
msgid ""
"This can serve as an alternative way to split out different parts of the "
"image, which is good for more painterly pieces:"
msgstr ""
"Це може слугувати альтернативним способом виділення частин на зображенні, а "
"це добре для створення фрагментів, які імітуватимуть малювання олійною "
"фарбою:"

#: ../../tutorials/flat-coloring.rst:249
msgid "A bit more precise than filling."
msgstr "Трохи точніше за заповнення."

#: ../../tutorials/flat-coloring.rst:254
msgid "Previewing your color isn't as easy."
msgstr "Попередній перегляд кольору не є таким простим."

#: ../../tutorials/flat-coloring.rst:257
msgid "Geometric tools"
msgstr "Геометричні інструменти"

#: ../../tutorials/flat-coloring.rst:259
msgid ""
"So you have a tool for making rectangles or circles. And in the case of "
"Krita, a tool for bezier curves. Select the path tool (|path tool|), and set "
"the tool options to fill=foreground and outline=none. Make sure that your "
"opacity is set to 1.00 (fully opaque)."
msgstr ""
"Отже, у вашому розпорядженні є інструмент для створення прямокутників або "
"кіл. І, у випадку Krita, ви маєте інструмент для створення кривих Безьє. "
"Виберіть інструмент контурів (|path tool|) і встановіть для параметрів "
"інструмента значення заповнення кольором переднього плану, а для контуру "
"значення «Немає». Встановіть значення непрозорості 1.00 (повна непрозорість)."

#: ../../tutorials/flat-coloring.rst:262
msgid ""
".. image:: images/icons/bezier_curve.svg\n"
"   :alt: path tool"
msgstr ""
".. image:: images/icons/bezier_curve.svg\n"
"   :alt: Інструмент малювання контурів"

#: ../../tutorials/flat-coloring.rst:264
msgid ""
"By clicking and holding, you can influence how curvy a line draw with the "
"path tool is going to be. Letting go of the mouse button confirms the "
"action, and then you’re free to draw the next point."
msgstr ""
"Клацанням і утриманням натиснутою кнопки миші ви можете впливати на те, "
"наскільки викривленою буде лінія контуру. Відпускання кнопки миші завершує "
"дію, і ви можете переходити до малювання наступної точки."

#: ../../tutorials/flat-coloring.rst:269
msgid ""
"You can also erase with a geometric tool. Just press the :kbd:`E` key or the "
"eraser button."
msgstr ""
"Ви також можете витирати геометричним інструментом. Просто натисніть "
"клавішу :kbd:`E` або кнопку гумки."

#: ../../tutorials/flat-coloring.rst:277
msgid ""
"Quicker than using the brush or selections. Also decent with line art that "
"contains gaps."
msgstr ""
"Швидше, ніж використання пензля або позначення ділянок. Крім того, добре "
"працює із графікою, у якій є прогалини."

#: ../../tutorials/flat-coloring.rst:282
msgid ""
"Fiddly details aren’t easy to fill in with this. So I recommend skipping "
"those and filling them in later with a brush."
msgstr ""
"Цим інструментом важко заповнювати малюсінькі деталі. Рекомендуємо не робити "
"цього і заповнити їх згодом за допомогою пензля."

#: ../../tutorials/flat-coloring.rst:285
msgid "Colorize Mask"
msgstr "Маска розфарбовування"

#: ../../tutorials/flat-coloring.rst:287
msgid ""
"So, this is a bit of an odd one. In the original tutorial, you'll see I'm "
"suggesting using G'Mic, but that was a few years ago, and G'Mic is a little "
"unstable on windows. Therefore, the Krita developers have been attempting to "
"make an internal tool doing the same."
msgstr ""
"Цей спосіб є дещо незвичним. У початковій версії підручника ви можете знайти "
"рекомендацію щодо використання G'Mic. Але це було декілька років тому, а "
"G'Mic працює дещо нестабільно у Windows. З цієї причини розробниками Krita "
"було зроблено спробу створення вбудованого інструмента, який би виконував ті "
"самі завдання."

#: ../../tutorials/flat-coloring.rst:289
msgid ""
"It is disabled in 3.1, but if you use 4.0 or later, it is in the toolbox. "
"Check the Colorize Mask for more information."
msgstr ""
"Цю можливість вимкнено у 3.1, але якщо ви працюєте із версією 4.0 або "
"новішою, відповідна кнопка є у наборі інструментів. Ознайомтеся із розділом "
"щодо масок розфарбовування, щоб дізнатися більше."

#: ../../tutorials/flat-coloring.rst:291
msgid "So it works like this:"
msgstr "Отже, маска працює так:"

#: ../../tutorials/flat-coloring.rst:293
msgid "Select the colorize mask tool."
msgstr "Виберіть інструмент «Маска розфарбовування»"

#: ../../tutorials/flat-coloring.rst:294
msgid "Tick the layer you're using."
msgstr "Позначте пункт шару, яким ви користуйтеся."

#: ../../tutorials/flat-coloring.rst:295
msgid "Paint the colors you want to use on the colorize mask"
msgstr ""
"Намалюйте щось кольорами, які ви використовуватимете для маски "
"розфарбовування"

#: ../../tutorials/flat-coloring.rst:296
msgid "Click update to see the results:"
msgstr "Натисніть кнопку :guilabel:`Оновити`, щоб переглянути результати:"

#: ../../tutorials/flat-coloring.rst:301
msgid ""
"When you are satisfied, |mouseright| the colorize mask, and go to :"
"menuselection:`Convert --> Paint Layer`. This will turn the colorize mask to "
"a generic paint layer. Then, you can fix the last issues by making the line "
"art semi-transparent and painting the flaws away with a pixel art brush."
msgstr ""
"Коли малювання буде завершено, клацніть |mouseright| на пункті маски "
"розфарбовування і скористайтеся пунктом меню :menuselection:`Перетворити --> "
"на шар малювання`. У результаті маску розфарбовування буде перетворено на "
"звичайний шар малювання. Потім ви можете виправити залишкові дефекти, "
"перетворивши графіку на напівпрозору і замалювавши дефекти за допомогою "
"пензля для піксель-арту."

#: ../../tutorials/flat-coloring.rst:306
msgid ""
"Then, when you are done, split the layers via :menuselection:`Layer --> "
"Split --> Split Layer`. There are a few options you can choose, but the "
"following should be fine:"
msgstr ""
"Потім, коли виправлення буде завершено, поділіть шари за допомогою пункту "
"меню :menuselection:`Шар --> Поділ --> Розділити шар`. Ви можете вибрати "
"значення декількох параметрів, але вказаних нижче значень має вистачити:"

#: ../../tutorials/flat-coloring.rst:311
msgid ""
"Finally, press **Ok** and you should get the following. Each color patch it "
"on a different layer, named by the palette in the menu and alpha locked, so "
"you can start painting right away!"
msgstr ""
"Нарешті, натисніть кнопку **Гаразд** і матимете таке: кожен мазок кольору на "
"своєму рівні, названому за палітрою у меню, із заблокованою прозорістю. "
"Тепер можна починати малювати!"

#: ../../tutorials/flat-coloring.rst:319
msgid ""
"Works with anti-aliased line art. Really quick to get the base work done. "
"Can auto-close gaps."
msgstr ""
"Працює зі згладженою графікою. Можна дуже швидко отримати базовий результат. "
"Може автоматично закривати прогалини."

#: ../../tutorials/flat-coloring.rst:324
msgid ""
"No anti-aliasing of its own. You have to choose between getting details "
"right or the gaps auto-closed."
msgstr ""
"Не має власних інструментів згладжування. Вам доведеться вибрати між "
"належним відтворенням деталей та автоматичним заповненням прогалин."

#: ../../tutorials/flat-coloring.rst:327
msgid "Conclusion"
msgstr "Висновки"

#: ../../tutorials/flat-coloring.rst:329
msgid ""
"I hope this has given you a good idea of how to fill in flats using the "
"various techniques, as well as getting a hand of different Krita features. "
"Remember that a good flat filled line art is better than a badly shaded one, "
"so keep practicing to get the best out of these techniques!"
msgstr ""
"Сподіваємося, ви зрозуміли базові способи зафарбовування за допомогою різних "
"методик, а також дізналися більше про різні можливості Krita. Пам'ятайте, що "
"якісно розфарбована простими кольорами графіка виглядає краще за абияк "
"виконані тіні. Тому практикуйтеся і користуйтеся тією методикою, якою "
"володієте краще!"
