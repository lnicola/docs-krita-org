# Translation of docs_krita_org_reference_manual___preferences___shortcut_settings.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___preferences___shortcut_settings\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:50+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "Saving, loading and sharing custom shortcuts"
msgstr ""
"Збереження, завантаження і оприлюднення нетипових наборів клавіатурних "
"скорочень"

#: ../../reference_manual/preferences/shortcut_settings.rst:1
msgid "Configuring shortcuts in Krita."
msgstr "Налаштовування клавіатурних скорочень у Krita."

#: ../../reference_manual/preferences/shortcut_settings.rst:11
msgid "Preferences"
msgstr "Налаштування"

#: ../../reference_manual/preferences/shortcut_settings.rst:11
msgid "Settings"
msgstr "Параметри"

#: ../../reference_manual/preferences/shortcut_settings.rst:11
msgid "Shortcuts"
msgstr "Клавіатурні скорочення"

#: ../../reference_manual/preferences/shortcut_settings.rst:16
msgid "Shortcut Settings"
msgstr "Параметри клавіатурних скорочень"

#: ../../reference_manual/preferences/shortcut_settings.rst:18
msgid ""
"Most of Krita's shortcuts are configured in the menu section :menuselection:"
"`Settings --> Configure Krita --> Configure Shortcuts`. The shortcuts "
"configured here are simple key combinations, for example the :kbd:`Ctrl + X` "
"shortcut to cut. Shortcuts can also be sequences of key combinations (e.g. :"
"kbd:`Shift + S` shortcut then the :kbd:`B` key). Krita also has a special "
"interface for configuring the mouse and stylus events sent to the canvas, "
"found under :ref:`canvas_input_settings`."
msgstr ""
"Більшість клавіатурних скорочень у Krita можна налаштувати за допомогою "
"розділу меню :menuselection:`Параметри --> Налаштувати Krita --> Налаштувати "
"скорочення`. Клавіатурні скорочення — прості комбінації клавіш, наприклад :"
"kbd:`Ctrl + X` для дії вирізання. Скорочення також можуть бути "
"послідовностями комбінацій клавіш (наприклад :kbd:`Shift + S`, потім :kbd:"
"`B`). У Krita також передбачено спеціальний інтерфейс для налаштовування "
"подій на полотні, пов'язаних із мишею та стилом, опис якого можна знайти у "
"розділі :ref:`canvas_input_settings`."

#: ../../reference_manual/preferences/shortcut_settings.rst:21
msgid "Menu Items"
msgstr "Пункти меню"

#: ../../reference_manual/preferences/shortcut_settings.rst:23
msgid "Search bar"
msgstr "Панель пошуку"

#: ../../reference_manual/preferences/shortcut_settings.rst:24
msgid ""
"Entering text here will search for matching shortcuts in the shortcut list."
msgstr ""
"Якщо ви введете до цього поля критерій пошуку, програма залишить у списку "
"лише пункти, які відповідають критерію пошуку."

#: ../../reference_manual/preferences/shortcut_settings.rst:25
msgid "Shortcut List"
msgstr "Список скорочень"

#: ../../reference_manual/preferences/shortcut_settings.rst:26
msgid ""
"Shortcuts are organized into sections. Each shortcut can be given a primary "
"and alternate key combination"
msgstr ""
"Клавіатурні скорочення упорядковано за розділами. Для кожного пункту можна "
"визначити основну та альтернативну комбінацію клавіш."

#: ../../reference_manual/preferences/shortcut_settings.rst:28
msgid "Load/Save Shortcuts Profiles"
msgstr "Завантаження і збереження профілів скорочень"

#: ../../reference_manual/preferences/shortcut_settings.rst:28
msgid ""
"The bottom row of buttons contains commands for exporting and import "
"keyboard shortcuts."
msgstr ""
"У нижній частині вікна розташовано кнопки для експортування та імпортування "
"наборів клавіатурних скорочень."

#: ../../reference_manual/preferences/shortcut_settings.rst:31
msgid ".. image:: images/preferences/Krita_Configure_Shortcuts.png"
msgstr ".. image:: images/preferences/Krita_Configure_Shortcuts.png"

#: ../../reference_manual/preferences/shortcut_settings.rst:33
msgid "Configuration"
msgstr "Налаштування"

#: ../../reference_manual/preferences/shortcut_settings.rst:36
msgid "Primary and alternate shortcuts"
msgstr "Основне і альтернативне скорочення"

#: ../../reference_manual/preferences/shortcut_settings.rst:36
msgid ""
"Each shortcut is assigned a default, which may be empty. The user can assign "
"up to two custom shortcuts, known as primary and alternate shortcuts. Simply "
"click on a \"Custom\" button and type the key combination you wish to assign "
"to the shortcut. If the key combination is already in use for another "
"shortcut, the dialog will prompt the user to resolve the conflict."
msgstr ""
"У кожного клавіатурного скорочення є типове значення, яке може бути "
"порожнім. Користувач може призначити до двох нетипових скорочень, які "
"називаються основним та альтернативним. Просто натисніть пункт «Нетипове» і "
"комбінацію клавіш, яку ви хочете пов'язати із відповідною дією. Якщо цю "
"комбінацію клавіш вже пов'язано із іншою дією, програма покаже діалогове "
"вікно, у якому ви зможете розв'язати конфлікт між скороченнями."

#: ../../reference_manual/preferences/shortcut_settings.rst:39
msgid "Shortcut schemes"
msgstr "Схеми скорочень"

#: ../../reference_manual/preferences/shortcut_settings.rst:39
msgid ""
"Many users migrate to Krita from other tools with different default "
"shortcuts. Krita users may change the default shortcuts to mimic these other "
"programs.  Currently, Krita ships with defaults for Photoshop and Paint Tool "
"Sai. Additional shortcut schemes can be placed in the ~/.config/krita/input/ "
"folder."
msgstr ""
"Багато користувачів переходять на Krita з інших програм, де передбачено інші "
"типові клавіатурні скорочення. Користувачі Krita можуть змінювати типові "
"клавіатурні скорочення так, щоб вони збігалися із клавіатурними скороченнями "
"інших програм. У поточній версії до складу пакунка Krita входять набори, які "
"імітують Photoshop та Paint Tool Sai. Додаткові схеми клавіатурних скорочень "
"можна зберігати у теці ~/.config/krita/input/."

#: ../../reference_manual/preferences/shortcut_settings.rst:42
msgid ""
"Users may wish to export their shortcuts to use across machines, or even "
"share with other users. This can be done with the save/load drop-down. Note: "
"the shortcuts can be saved and overridden manually by backingup the text "
"file kritashortcutsrc located in ~/.config/krita/.  Additionally, the user "
"can export a custom shortcut scheme file generated by merging the existing "
"scheme defaults with the current customizations."
msgstr ""
"У користувачів може виникнути бажання експортувати створені схеми "
"клавіатурних скорочень для використання на інших комп'ютерах або навіть "
"оприлюднення для використання іншими користувачами. Виконати експортування "
"або завантаження скорочень можна за допомогою спадного списку зберігання/"
"завантаження. Зауваження: клавіатурні скорочення можна зберігати і "
"перевизначати вручну за допомогою текстового файла kritashortcutsrc, який "
"зберігається у теці ~/.config/krita/. Крім того, користувачі можуть "
"експортувати файл нетипової схеми клавіатурних скорочень, створений "
"об'єднанням наявної типової схеми із поточними змінами."
