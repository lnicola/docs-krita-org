# Translation of docs_krita_org_reference_manual___tools___perspective_grid.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___tools___perspective_grid\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 16:41+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<rst_epilog>:50
msgid ""
".. image:: images/icons/perspectivegrid_tool.svg\n"
"   :alt: toolperspectivegrid"
msgstr ""
".. image:: images/icons/perspectivegrid_tool.svg\n"
"   :alt: toolperspectivegrid"

#: ../../reference_manual/tools/perspective_grid.rst:1
msgid "Krita's perspective grid tool reference."
msgstr "Довідник із інструмента ґратки перспективи Krita."

#: ../../reference_manual/tools/perspective_grid.rst:11
msgid "Tools"
msgstr "Інструменти"

#: ../../reference_manual/tools/perspective_grid.rst:11
msgid "Perspective"
msgstr "Перспектива"

#: ../../reference_manual/tools/perspective_grid.rst:11
msgid "Grid"
msgstr "Ґратка"

#: ../../reference_manual/tools/perspective_grid.rst:16
msgid "Perspective Grid Tool"
msgstr "Інструмент «Ґратка перспективи»"

#: ../../reference_manual/tools/perspective_grid.rst:18
msgid "|toolperspectivegrid|"
msgstr "|toolperspectivegrid|"

#: ../../reference_manual/tools/perspective_grid.rst:22
msgid "Deprecated in 3.0, use the :ref:`assistant_perspective` instead."
msgstr ""
"Є застарілим, починаючи з версії 3.0, користуйтеся замість цього інструмента "
"інструментом :ref:`assistant_perspective`."

#: ../../reference_manual/tools/perspective_grid.rst:24
msgid ""
"The perspective grid tool allows you to draw and manipulate grids on the "
"canvas that can serve as perspective guides for your painting. A grid can be "
"added to your canvas by first clicking the tool in the tool bar and then "
"clicking four points on the canvas which will serve as the four corners of "
"your grid."
msgstr ""
"Інструмент ґратки перспективи надає вам змогу малювати і редагувати ґратки "
"на полотні. Ці ґратки може бути використано як напрямні для малювання. Щоб "
"додати ґратку на полотно, спочатку натисніть кнопку інструмента на панелі "
"інструментів, потім клацніть у чотирьох точках на полотні, які будуть "
"чотирма кутами вашої ґратки."

#: ../../reference_manual/tools/perspective_grid.rst:27
msgid ".. image:: images/tools/Perspectivegrid.png"
msgstr ".. image:: images/tools/Perspectivegrid.png"

#: ../../reference_manual/tools/perspective_grid.rst:28
msgid ""
"The grid can be manipulated by pulling on any of its four corners. The grid "
"can be extended by clicking and dragging a midpoint of one of its edges. "
"This will allow you to expand the grid at other angles. This process can be "
"repeated on any subsequent grid or grid section. You can join the corners of "
"two grids by dragging one onto the other. Once they are joined they will "
"always move together, they cannot be separated. You can delete any grid by "
"clicking on the red X at its center. This tool can be used to build "
"reference for complex scenes."
msgstr ""
"Редагувати ґратку можна перетягуванням будь-якого з її чотирьох кутів. "
"Розширити чотирикутник ґратки можна за допомогою перетягування середньої "
"точки будь-якої з його сторін. Цим можна скористатися для розширення ґратки "
"у інших кутах. Процес можна повторювати для будь-якої підлеглої ґратки або "
"частини ґратки. З'єднати кути двох ґраток можна простим перетягуванням "
"кутових точок одна на одну. Після з'єднання у такий спосіб ці точки "
"рухатимуться разом, їх не можна буде роз'єднати. Вилучити будь-яку ґратку "
"можна натисканням червоного хрестика у її центрі. Цим інструментом можна "
"скористатися для побудови допоміжних ліній для складних сцен."

#: ../../reference_manual/tools/perspective_grid.rst:30
msgid "As displayed while the Perspective Grid tool is active: *"
msgstr "Показ у режимі інструмента ґратки перспективи:"

#: ../../reference_manual/tools/perspective_grid.rst:33
msgid ".. image:: images/tools/Multigrid.png"
msgstr ".. image:: images/tools/Multigrid.png"

#: ../../reference_manual/tools/perspective_grid.rst:34
msgid "As displayed while any other tool is active: *"
msgstr "Показ у режимі будь-якого іншого інструмента:"

#: ../../reference_manual/tools/perspective_grid.rst:37
msgid ".. image:: images/tools/KritaPersgridnoedit.png"
msgstr ".. image:: images/tools/KritaPersgridnoedit.png"

#: ../../reference_manual/tools/perspective_grid.rst:38
msgid ""
"You can toggle the visibility of the grid from the main menu :menuselection:"
"`View --> Show Perspective Grid` option. You can also clear any grid setup "
"you have and start over by using the :menuselection:`View --> Clear "
"Perspective Grid`."
msgstr ""
"Ви можете увімкнути видимість ґратки за допомогою пункту головного меню :"
"menuselection:`Перегляд --> Показати ґратку перспективи`. Ви також можете "
"вилучити створену вами ґратку і розпочати її створення з початку за "
"допомогою пункту меню :menuselection:`Перегляд --> Вилучити ґратку "
"перспективи`."
