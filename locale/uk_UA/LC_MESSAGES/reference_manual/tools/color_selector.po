# Translation of docs_krita_org_reference_manual___tools___color_selector.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___tools___color_selector\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:48+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/tools/color_selector.rst:1
msgid "Krita's color selector tool reference."
msgstr "Довідник із інструмента вибору кольорів Krita."

#: ../../reference_manual/tools/color_selector.rst:12
msgid "Tools"
msgstr "Інструменти"

#: ../../reference_manual/tools/color_selector.rst:12
msgid "Colors"
msgstr "Кольори"

#: ../../reference_manual/tools/color_selector.rst:12
msgid "Eyedropper"
msgstr "Піпетка"

#: ../../reference_manual/tools/color_selector.rst:12
msgid "Color Selector"
msgstr "Вибір кольору"

#: ../../reference_manual/tools/color_selector.rst:17
msgid "Color Selector Tool"
msgstr "Інструмент «Вибір кольору»"

#: ../../reference_manual/tools/color_selector.rst:20
msgid ""
"This tool allows you to choose a point from the canvas and make the color of "
"that point the active foreground color. When a painting or drawing tool is "
"selected the Color Picker tool can also be quickly accessed by pressing the :"
"kbd:`Ctrl` key."
msgstr ""
"За допомогою цього інструмента можна вибрати точку на полотні і зробити "
"колір цієї точки активним кольором переднього плану. Якщо вибрано інструмент "
"малювання або креслення, доступ до інструмента вибору кольору можна швидко "
"отримати натисканням клавіші :kbd:`Ctrl`."

#: ../../reference_manual/tools/color_selector.rst:23
msgid ".. image:: images/tools/Color_Dropper_Tool_Options.png"
msgstr ".. image:: images/tools/Color_Dropper_Tool_Options.png"

#: ../../reference_manual/tools/color_selector.rst:24
msgid ""
"There are several options shown in the :guilabel:`Tool Options` docker when "
"the :guilabel:`Color Picker` tool is active:"
msgstr ""
"На бічні панелі параметрів інструмента буде показано декілька параметрів, "
"якщо активним буде інструмент вибору кольору."

#: ../../reference_manual/tools/color_selector.rst:26
msgid ""
"The first drop-down box allows you to select whether you want to sample from "
"all visible layers or only the active layer. You can choose to have your "
"selection update the current foreground color, to be added into a color "
"palette, or to do both."
msgstr ""
"За допомогою першого спадного списку ви можете вибрати, звідки буде взято "
"зразок — з усіх видимих шарів чи лише з активного шару. Ви можете вибрати "
"один з варіантів: оновити поточний колір переднього тла, додати колір до "
"палітри або виконати обидві дії."

#: ../../reference_manual/tools/color_selector.rst:30
msgid ""
"The middle section contains a few properties that change how the Color "
"Picker picks up color; you can set a :guilabel:`Radius`, which will average "
"the colors in the area around the cursor, and you can now also set a :"
"guilabel:`Blend` percentage, which controls how much color is \"soaked up\" "
"and mixed in with your current color. Read :ref:`mixing_colors` for "
"information about how the Color Picker's blend option can be used as a tool "
"for off-canvas color mixing."
msgstr ""
"У середній частині міститься декілька властивостей, які змінюють спосіб, у "
"який піпетка визначає колір. Ви можете встановити :guilabel:`Радіус`, за "
"яким осереднюватиметься значення кольору у ділянці навколо курсора, і ви "
"можете встановити відсоток :guilabel:`Змішування`, який визначатиме, яку "
"частку кольору буде «всмоктано» і змішано з поточним кольором. Ознайомтеся "
"із вмістом розділу :ref:`mixing_colors`, щоб дізнатися про те, які параметр "
"змішування кольорів для піпетки можна використати як інструмент для "
"змішування кольорів поза полотном."

#: ../../reference_manual/tools/color_selector.rst:32
msgid ""
"At the very bottom is the Info Box, which displays per-channel data about "
"your most recently picked color. Color data can be shown as 8-bit numbers or "
"percentages."
msgstr ""
"У нижній частині вікна розташовано інформаційну панель, на якій показано "
"дані для кожного з каналів останнього вибраного кольору. Дані кольору може "
"бути показано у форматі 8-бітових чисел або відсотків."
