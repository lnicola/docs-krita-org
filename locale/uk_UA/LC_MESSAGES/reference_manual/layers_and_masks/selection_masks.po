# Translation of docs_krita_org_reference_manual___layers_and_masks___selection_masks.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___layers_and_masks___selection_masks\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:48+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../reference_manual/layers_and_masks/selection_masks.rst:1
msgid "How to use selection masks in Krita."
msgstr "Як користуватися масками позначення у Krita."

#: ../../reference_manual/layers_and_masks/selection_masks.rst:15
msgid "Layers"
msgstr "Шари"

#: ../../reference_manual/layers_and_masks/selection_masks.rst:15
msgid "Masks"
msgstr "Маски"

#: ../../reference_manual/layers_and_masks/selection_masks.rst:15
msgid "Selections"
msgstr "Позначення"

#: ../../reference_manual/layers_and_masks/selection_masks.rst:20
msgid "Selection Masks"
msgstr "Маски позначення"

#: ../../reference_manual/layers_and_masks/selection_masks.rst:22
msgid ""
"Local Selection masks let you remember and recall edit a selection on a "
"layer. They work in a similar way to extra channels in other image editing "
"programs. One difference is :program:`Krita's` ability to assign them to "
"specific layers and activate a selection with a single click on the layer. "
"Just click the round icon with the dotted outline on the local selection "
"layer in the Layers docker."
msgstr ""
"Локальні маски позначення надають вам змогу зберігати і відновлювати "
"редагування позначеної ділянки на шарі. Ці маски працюють подібно до "
"додаткових каналів у інших програмах для редагування зображень. Єдиною "
"відмінністю є те, що у :program:`Krita` передбачено можливість пов'язування "
"цих масок із окремими шарами та активація позначення одним клацанням на "
"пункті шару. Достатньо натиснути кнопку із піктограмою з кружком із "
"пунктирним контуром на пункті шару локального позначення бічної панелі шарів."

#: ../../reference_manual/layers_and_masks/selection_masks.rst:24
msgid ""
"You can make them by making a selection, and |mouseright| the layer you want "
"to add it to select :menuselection:`Local Selection`."
msgstr ""
"Створити локальну маску позначення можна позначенням ділянки зображення із "
"наступним клацанням |mouseright| на пункті шару, до якого ви її хочете "
"додати, і вибором пункту :menuselection:`Локальне позначення`."

#: ../../reference_manual/layers_and_masks/selection_masks.rst:26
msgid ""
"When isolating a selection mask with the :kbd:`Alt +` |mouseleft| shortcut, "
"you can perform transformation, deformation and paint operations on the "
"selection layer, modifying the selection."
msgstr ""
"Якщо ізолювати маску позначення за допомогою комбінації :kbd:`Alt`  + |"
"mouseleft|, ви зможете виконувати дії з перетворення, деформування та "
"малювання на шарі позначення, вносячи зміни до вигляду позначеної ділянки "
"зображення."

#: ../../reference_manual/layers_and_masks/selection_masks.rst:28
msgid ""
"A single layer can contain multiple Local Selection Masks. Repeating. A "
"single layer can contain multiple Local Selection Masks (LSM). This is "
"important because it means that you can, for instance, have several "
"different outline parts of an image and save each as its own LSM and then "
"recall it with a single click. Without using LSM you would have to create "
"layer upon layer for each mask. Not only would this be inefficient for you "
"but also for Krita and the program would slow down trying to keep up with it "
"all. LSM's are one of the most important features in Krita!"
msgstr ""
"Один шар може містити декілька масок локального позначення. Ще раз. Один шар "
"може містити декілька масок локального позначення (LSM). Це важливо, "
"оскільки це означає, що ви можете, наприклад, створювати декілька різних "
"позначених частин зображення і зберігати кожну у власній LSM, а потім "
"викликати одним клацанням кнопкою миші. Без LSM довелося б створювати шар "
"для кожної маски. Це не лише було б неефективно з вашої точки зору, але це "
"було б неефективно з точки зору Krita, оскільки робота програми "
"уповільнювалася через потребу у ресурсах для кожної з масок. LSM — є однією "
"з найважливіших можливостей Krita!"

#: ../../reference_manual/layers_and_masks/selection_masks.rst:30
msgid ""
"The example below shows three LSM items all attached (under) Layer1. Any of "
"these can be activated and used at any time."
msgstr ""
"У прикладі, який наведено нижче, три пункти маски локального позначення "
"пов'язано (знизу) із шаром Layer1. Будь-яку з цих масок можна будь-коли "
"активувати і використати."

#: ../../reference_manual/layers_and_masks/selection_masks.rst:34
msgid "Global Selection"
msgstr "Загальне позначення"

#: ../../reference_manual/layers_and_masks/selection_masks.rst:36
msgid ""
"You can modify the global selection the same way you can with a local-"
"selection. To do so, you first need to activate the global selection as a "
"layer node. To do so, go into :menuselection:`Select --> Show Global "
"Selection Mask`. The global selection, if you have anything selected, will "
"now appear on the top of the layer stack as a selection mask."
msgstr ""
"Ви можете вносити зміни до загального позначення у той самий спосіб, у який "
"ви можете змінювати локальне позначення. Для цього вам спочатку слід "
"активувати загальне позначення як вузол списку шарів. Для цього "
"скористайтеся пунктом меню :menuselection:`Вибір --> Показати маску "
"загального вибору`. Якщо щось позначено, після цього у верхній частині "
"списку шарів буде показано пункт маски загального вибору."
