# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:11+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<generated>:1
msgid "Separate Image"
msgstr "Separera bild"

#: ../../reference_manual/main_menu/image_menu.rst:1
msgid "The image menu in Krita."
msgstr "Menyn Bild i Krita."

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Image"
msgstr "Bild"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Canvas Projection Color"
msgstr "Dukprojektionsfärg"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Trim"
msgstr "Beskära"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Resize"
msgstr "Ändra storlek"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Scale"
msgstr "Skala"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Mirror"
msgstr "Spegla"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Transform"
msgstr "Transformera"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Convert Color Space"
msgstr "Konvertera färgrymd"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Offset"
msgstr "Position"

#: ../../reference_manual/main_menu/image_menu.rst:11
msgid "Split Channels"
msgstr "Dela kanaler"

#: ../../reference_manual/main_menu/image_menu.rst:16
msgid "Image Menu"
msgstr "Menyn Bild"

#: ../../reference_manual/main_menu/image_menu.rst:18
msgid "Properties"
msgstr "Egenskaper"

#: ../../reference_manual/main_menu/image_menu.rst:19
msgid "Gives you the image properties."
msgstr "Ger dig bildegenskaperna."

#: ../../reference_manual/main_menu/image_menu.rst:20
msgid "Image Background Color and Transparency"
msgstr "Bildens bakgrundsfärg och genomskinlighet"

#: ../../reference_manual/main_menu/image_menu.rst:21
msgid "Change the background canvas color."
msgstr "Ändra dukens bakgrundsfärg."

#: ../../reference_manual/main_menu/image_menu.rst:22
msgid "Convert Current Image Color Space."
msgstr "Konvertera den aktuella bildens färgrymd."

#: ../../reference_manual/main_menu/image_menu.rst:23
msgid "Converts the current image to a new colorspace."
msgstr "Konvertera den aktuella bilden till en ny färgrymd."

#: ../../reference_manual/main_menu/image_menu.rst:24
msgid "Trim to image size"
msgstr "Beskär till bildstorlek"

#: ../../reference_manual/main_menu/image_menu.rst:25
msgid ""
"Trims all layers to the image size. Useful for reducing filesize at the loss "
"of information."
msgstr ""
"Beskär alla lager till bildstorleken. Användbart för att reducera "
"filstorleken även om information går förlorad."

#: ../../reference_manual/main_menu/image_menu.rst:26
msgid "Trim to Current Layer"
msgstr "Beskär till nuvarande lager"

#: ../../reference_manual/main_menu/image_menu.rst:27
msgid ""
"A lazy cropping function. Krita will use the size of the current layer to "
"determine where to crop."
msgstr ""
"En lat beskärningsfunktion. Krita använder det nuvarande lagrets storlek för "
"att bestämma var beskärningen ska göras."

#: ../../reference_manual/main_menu/image_menu.rst:28
msgid "Trim to Selection"
msgstr "Beskär till markering"

#: ../../reference_manual/main_menu/image_menu.rst:29
msgid ""
"A lazy cropping function. Krita will crop the canvas to the selected area."
msgstr "En lat beskärningsfunktion. Krita beskär duken till markerat område."

#: ../../reference_manual/main_menu/image_menu.rst:30
msgid "Rotate Image"
msgstr "Rotera bild"

#: ../../reference_manual/main_menu/image_menu.rst:31
msgid "Rotate the image"
msgstr "Rotera bilden"

#: ../../reference_manual/main_menu/image_menu.rst:32
msgid "Shear Image"
msgstr "Skjuva bild"

#: ../../reference_manual/main_menu/image_menu.rst:33
msgid "Shear the image"
msgstr "Skjuva bilden"

#: ../../reference_manual/main_menu/image_menu.rst:34
msgid "Mirror Image Horizontally"
msgstr "Spegla bild horisontellt"

#: ../../reference_manual/main_menu/image_menu.rst:35
msgid "Mirror the image on the horizontal axis."
msgstr "Spegla bilden längs den horisontella axeln."

#: ../../reference_manual/main_menu/image_menu.rst:36
msgid "Mirror Image Vertically"
msgstr "Spegla bild vertikalt"

#: ../../reference_manual/main_menu/image_menu.rst:37
msgid "Mirror the image on the vertical axis."
msgstr "Spegla bilden längs den vertikala axeln."

#: ../../reference_manual/main_menu/image_menu.rst:38
msgid "Scale to New Size"
msgstr "Skala till ny storlek"

#: ../../reference_manual/main_menu/image_menu.rst:39
msgid ""
"The resize function in any other program with the :kbd:`Ctrl + Alt + I` "
"shortcut."
msgstr ""
"Storleksändringsfunktionen i andra program med genvägen :kbd:`Ctrl + Alt + "
"I`."

#: ../../reference_manual/main_menu/image_menu.rst:40
msgid "Offset Image"
msgstr "Positionera bild"

#: ../../reference_manual/main_menu/image_menu.rst:41
msgid "Offset all layers."
msgstr "Positionera alla lager."

#: ../../reference_manual/main_menu/image_menu.rst:42
msgid "Resize Canvas"
msgstr "Ändra storlek på duk"

#: ../../reference_manual/main_menu/image_menu.rst:43
msgid "Change the canvas size. Don't confuse this with Scale to new size."
msgstr "Ändra dukens storlek. Blanda inte ihop det med Skala till ny storlek."

#: ../../reference_manual/main_menu/image_menu.rst:44
msgid "Image Split"
msgstr "Dela bild"

#: ../../reference_manual/main_menu/image_menu.rst:45
msgid "Calls up the :ref:`image_split` dialog."
msgstr "Visar dialogrutan :ref:`image_split`."

#: ../../reference_manual/main_menu/image_menu.rst:46
msgid "Wavelet Decompose"
msgstr "Uppdelning med vågelement"

#: ../../reference_manual/main_menu/image_menu.rst:47
msgid "Does :ref:`wavelet_decompose` on the current layer."
msgstr "Gör :ref:`wavelet_decompose` med det nuvarande lagret."

#: ../../reference_manual/main_menu/image_menu.rst:49
msgid ":ref:`Separates <separate_image>` the image into channels."
msgstr ":ref:`Separerar <separate_image>` bilden i kanaler."
