# Translation of docs_krita_org_general_concepts___file_formats___file_kpl.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: general_concepts\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 13:42+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../general_concepts/file_formats/file_kpl.rst:1
msgid "The Krita Palette file format."
msgstr "El format de fitxer paleta del Krita."

#: ../../general_concepts/file_formats/file_kpl.rst:10
msgid "*.kpl"
msgstr "*.kpl"

#: ../../general_concepts/file_formats/file_kpl.rst:10
msgid "KPL"
msgstr "KPL"

#: ../../general_concepts/file_formats/file_kpl.rst:10
msgid "Krita Palette"
msgstr "Paleta del Krita"

#: ../../general_concepts/file_formats/file_kpl.rst:15
msgid "\\*.kpl"
msgstr "\\*.kpl"

# skip-rule: t-acc_obe
#: ../../general_concepts/file_formats/file_kpl.rst:17
msgid ""
"Since 4.0, Krita has a new palette file-format that can handle colors that "
"are wide gamut, RGB, CMYK, XYZ, GRAY, or LAB, and can be of any of the "
"available bitdepths, as well as groups. These are Krita Palettes, or ``*."
"kpl``."
msgstr ""
"Des de la versió 4.0, el Krita té un nou format de fitxer de paleta que pot "
"gestionar els colors de gamma àmplia, RGB, CMYK, XYZ, GRAY o LAB, i pot ser "
"de qualsevol de les profunditats de bits disponibles, així com de grups. Són "
"les paletes del Krita o ``*.kpl``."

# skip-rule: t-acc_obe
#: ../../general_concepts/file_formats/file_kpl.rst:19
msgid ""
"``*.kpl`` files are ZIP files, with two XMLs and ICC profiles inside. The "
"colorset XML contains the swatches as ColorSetEntry and Groups as Group. The "
"profiles.XML contains a list of profiles, and the ICC profiles themselves "
"are embedded to ensure compatibility over different computers."
msgstr ""
"Els fitxers ``*.kpl`` són fitxers ZIP, amb dos XML i perfils ICC a "
"l'interior. El XML per al conjunt de colors conté les mostres com "
"«ColorSetEntry» i «Groups» com a Grup. El «profiles.XML» conté una llista "
"dels perfils i els mateixos perfils ICC estan incrustats per a garantir la "
"compatibilitat entre diferents ordinadors."
