# Translation of docs_krita_org_reference_manual___audio_for_animation.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 15:20+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/audio_for_animation.rst:1
msgid "The audio playback with animation in Krita."
msgstr "La reproducció d'àudio amb animació al Krita."

#: ../../reference_manual/audio_for_animation.rst:12
msgid "Animation"
msgstr "Animació"

#: ../../reference_manual/audio_for_animation.rst:12
msgid "Audio"
msgstr "Àudio"

#: ../../reference_manual/audio_for_animation.rst:12
msgid "Sound"
msgstr "So"

#: ../../reference_manual/audio_for_animation.rst:12
msgid "Timeline"
msgstr "Línia de temps"

#: ../../reference_manual/audio_for_animation.rst:17
msgid "Audio for Animation"
msgstr "Àudio per a l'animació"

#: ../../reference_manual/audio_for_animation.rst:21
msgid ""
"Audio for animation is an unfinished feature. It has multiple bugs and may "
"not work on your system."
msgstr ""
"L'àudio per a l'animació és una característica sense acabar. Té múltiples "
"errors i és possible que no funcioni en el vostre sistema."

#: ../../reference_manual/audio_for_animation.rst:23
msgid ""
"You can add audio files to your animation to help sync lips or music. This "
"functionality is available in the timeline docker."
msgstr ""
"Podeu afegir fitxers d'àudio a la vostra animació per ajudar a sincronitzar "
"els llavis o la música. Aquesta funcionalitat està disponible a l'acoblador "
"Línia de temps."

#: ../../reference_manual/audio_for_animation.rst:26
msgid "Importing Audio Files"
msgstr "Importar fitxers d'àudio"

#: ../../reference_manual/audio_for_animation.rst:28
msgid ""
"Krita supports MP3, OGM, and WAV audio files. When you open up your timeline "
"docker, there will be a speaker button in the top left area."
msgstr ""
"El Krita admet fitxers d'àudio MP3, OGM i WAV. Quan obriu l'acoblador Línia "
"de temps, hi haurà un botó d'altaveu a la part superior esquerra."

# skip-rule: kct-button
#: ../../reference_manual/audio_for_animation.rst:30
msgid ""
"If you press the speaker button, you will get the available audio options "
"for the animation."
msgstr ""
"Si el premeu, obtindreu les opcions d'àudio disponibles per a l'animació."

#: ../../reference_manual/audio_for_animation.rst:32
msgid "Open"
msgstr "Obre"

#: ../../reference_manual/audio_for_animation.rst:33
msgid "Mute"
msgstr "Silenci"

#: ../../reference_manual/audio_for_animation.rst:34
msgid "Remove audio"
msgstr "Elimina l'àudio"

#: ../../reference_manual/audio_for_animation.rst:35
msgid "Volume slider"
msgstr "Control lliscant del volum"

#: ../../reference_manual/audio_for_animation.rst:37
msgid ""
"Krita saves the location of your audio file. If you move the audio file or "
"rename it, Krita will not be able to find it. Krita will tell you the file "
"was moved or deleted the next time you try to open the Krita file up."
msgstr ""
"El Krita desa la ubicació del vostre fitxer d'àudio. Si el moveu o li "
"canvieu el nom, el Krita no el podrà trobar. La propera vegada que intenteu "
"obrir el fitxer, el Krita us indicarà que s'ha mogut o suprimit."

#: ../../reference_manual/audio_for_animation.rst:40
msgid "Using Audio"
msgstr "Emprar l'àudio"

#: ../../reference_manual/audio_for_animation.rst:42
msgid ""
"After you import the audio, you can scrub through the timeline and it will "
"play the audio chunk at the time spot. When you press the Play button, the "
"entire the audio file will playback as it will in the final version. There "
"is no visual display of the audio file on the screen, so you will need to "
"use your ears and the scrubbing functionality to position frames."
msgstr ""
"Després d'importar l'àudio, podreu rastrejar la línia de temps i al mateix "
"temps reproduir el fragment d'àudio. Quan premeu el botó Reprodueix, es "
"reproduirà tot el fitxer d'àudio tal com es farà en la versió final. No hi "
"ha cap mostra visual del fitxer d'àudio a la pantalla, de manera que haureu "
"d'utilitzar les orelles i la funcionalitat de rastrejar per a posicionar els "
"fotogrames."

#: ../../reference_manual/audio_for_animation.rst:46
msgid "Exporting with Audio"
msgstr "Exportar amb àudio"

#: ../../reference_manual/audio_for_animation.rst:48
msgid ""
"To get the audio file included when you are exporting, you need to include "
"it in the Render Animation options. In the :menuselection:`File --> Render "
"Animation` options there is a checkbox :guilabel:`Include Audio`. Make sure "
"that is checked before you export and you should be good to go."
msgstr ""
"Per aconseguir que el fitxer d'àudio sigui inclòs quan exporteu, l'haureu "
"d'incloure a les opcions Renderitza l'animació. A les opcions de :"
"menuselection:`Fitxer --> Renderitza l'animació` hi ha una casella de "
"selecció :guilabel:`Inclou l'àudio`. Assegureu-vos que s'ha marcat abans "
"d'exportar i hauria d'anar bé."

#: ../../reference_manual/audio_for_animation.rst:51
msgid "Packages needed for Audio on Linux"
msgstr "Paquets necessaris per a l'àudio a Linux"

#: ../../reference_manual/audio_for_animation.rst:53
msgid ""
"The following packages are necessary for having the audio support on Linux:"
msgstr ""
"Els següents paquets són necessaris per tenir el suport d'àudio a Linux:"

#: ../../reference_manual/audio_for_animation.rst:56
msgid "For people who build Krita on Linux:"
msgstr "Per a les persones que construeixen el Krita a Linux:"

#: ../../reference_manual/audio_for_animation.rst:58
msgid "libasound2-dev"
msgstr "libasound2-dev"

#: ../../reference_manual/audio_for_animation.rst:59
msgid "libgstreamer1.0-dev gstreamer1.0-pulseaudio"
msgstr "libgstreamer1.0-dev gstreamer1.0-pulseaudio"

#: ../../reference_manual/audio_for_animation.rst:60
msgid "libgstreamer-plugins-base1.0-dev"
msgstr "libgstreamer-plugins-base1.0-dev"

#: ../../reference_manual/audio_for_animation.rst:61
msgid "libgstreamer-plugins-good1.0-dev"
msgstr "libgstreamer-plugins-good1.0-dev"

#: ../../reference_manual/audio_for_animation.rst:62
msgid "libgstreamer-plugins-bad1.0-dev"
msgstr "libgstreamer-plugins-bad1.0-dev"

#: ../../reference_manual/audio_for_animation.rst:64
msgid "For people who use Krita on Linux:"
msgstr "Per a les persones que utilitzen el Krita a Linux:"

#: ../../reference_manual/audio_for_animation.rst:66
msgid "libqt5multimedia5-plugins"
msgstr "libqt5multimedia5-plugins"

#: ../../reference_manual/audio_for_animation.rst:67
msgid "libgstreamer-plugins-base1.0"
msgstr "libgstreamer-plugins-base1.0"

#: ../../reference_manual/audio_for_animation.rst:68
msgid "libgstreamer-plugins-good1.0"
msgstr "libgstreamer-plugins-good1.0"

#: ../../reference_manual/audio_for_animation.rst:69
msgid "libgstreamer-plugins-bad1.0"
msgstr "libgstreamer-plugins-bad1.0"
