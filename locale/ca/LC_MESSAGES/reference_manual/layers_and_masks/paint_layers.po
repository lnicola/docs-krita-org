# Translation of docs_krita_org_reference_manual___layers_and_masks___paint_layers.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-07 00:24+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../reference_manual/layers_and_masks/paint_layers.rst:1
msgid "How to use paint layers in Krita."
msgstr "Com emprar les capes de pintura en el Krita."

#: ../../reference_manual/layers_and_masks/paint_layers.rst:15
msgid "Layers"
msgstr "Capes"

#: ../../reference_manual/layers_and_masks/paint_layers.rst:15
msgid "Paint Layer"
msgstr "Capa de pintura"

#: ../../reference_manual/layers_and_masks/paint_layers.rst:15
msgid "Raster"
msgstr "Ràster"

#: ../../reference_manual/layers_and_masks/paint_layers.rst:15
msgid "Bitmap"
msgstr "Mapa de bits"

#: ../../reference_manual/layers_and_masks/paint_layers.rst:20
msgid "Paint Layers"
msgstr "Capes de pintura"

#: ../../reference_manual/layers_and_masks/paint_layers.rst:22
msgid ""
"Paint layers are the most commonly used type of layers used in digital paint "
"or image manipulation software like Krita. If you've ever used layers in :"
"program:`Photoshop` or the :program:`Gimp`, you'll be used to how they work. "
"In short, a paint layer, also called a pixel, bitmap or raster layer, is a "
"bitmap image (an image made up of many points of color)."
msgstr ""
"Les capes de pintura són el tipus de capa més utilitzat en la pintura "
"digital o el programari de manipulació d'imatges com el Krita. Si alguna "
"vegada heu utilitzat les capes en el :program:`Photoshop` o el :program:"
"`Gimp`, estareu acostumat a com funcionen. En resum, una capa de pintura, "
"també anomenada capa de píxels, de mapa de bits o ràster, és una imatge de "
"mapa de bits (una imatge composta de molts punts de color)."

#: ../../reference_manual/layers_and_masks/paint_layers.rst:24
msgid ""
"Paint layers let you apply many advanced effects such as smearing, smudging "
"and distorting. This makes them the most flexible type of layer. However,  "
"paint layers don't scale well when enlarged (they pixelate), and any effects "
"that have been applied can't be edited."
msgstr ""
"Les capes de pintura permeten aplicar molts efectes avançats com untat, "
"esborronat i distorsions. Això les converteix en el tipus de capa més "
"flexible. No obstant això, les capes de pintura no s'escalen bé quan "
"s'amplien (es pixelen), i no es poden editar els efectes que s'han aplicat."

#: ../../reference_manual/layers_and_masks/paint_layers.rst:26
msgid ""
"To deal with these two drawbacks, digital artists will typically work at "
"higher Pixel Per Inch (PPI) counts.  It is not unusual to see PPI settings "
"of 400 to 600 PPI for a canvas with a good amount of detail.  To combat the "
"issue of applied effects that cannot be edited it is best to take advantage "
"of the non-destructive layer capabilities of filter, transparency and "
"transform masks."
msgstr ""
"Per a bregar amb aquests dos inconvenients, els artistes digitals normalment "
"treballaran en recomptes més alts dels píxels per polzada (PPP). No és "
"estrany veure ajustaments de 400 fins a 600 ppp per a un llenç amb una bona "
"quantitat de detalls. Per a combatre el problema que no es podran editar els "
"efectes aplicats, el millor és aprofitar les capacitats de capa no "
"destructiva de les màscares de filtratge, de transparència i de "
"transformació."

#: ../../reference_manual/layers_and_masks/paint_layers.rst:28
msgid ""
"As long as you have enough resolution / size on your canvas though, and as "
"long as you aren't going to need to go back and tweak an effect you created "
"previously, then a paint layer is usually the type of layer you will want. "
"If you click on the :guilabel:`New layer` icon in the layers docker you'll "
"get a paint layer. Of course you can always choose the :guilabel:`New layer` "
"drop-down to get another type."
msgstr ""
"No obstant això, sempre que tingueu prou resolució / mida en el vostre llenç "
"i no hàgiu de tornar enrere i modificar l'efecte que heu creat, llavors una "
"capa de pintura generalment serà el tipus de capa que voldreu. Si feu clic "
"dret sobre la icona :guilabel:`Capa nova` a l'acoblador Capes, obtindreu una "
"capa de pintura. Per descomptat, sempre podreu triar el desplegable :"
"guilabel:`Capa nova` per obtenir els altres tipus."

#: ../../reference_manual/layers_and_masks/paint_layers.rst:30
msgid "The hotkey for adding a new paint layer is the :kbd:`Ins` key."
msgstr ""
"La drecera per afegir una capa de pintura nova és la tecla :kbd:`Inser`."
