# Translation of docs_krita_org_reference_manual___tools___move.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-24 16:20+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<generated>:1
msgid "Position"
msgstr "Posició"

#: ../../<rst_epilog>:44
msgid ""
".. image:: images/icons/move_tool.svg\n"
"   :alt: toolmove"
msgstr ""
".. image:: images/icons/move_tool.svg\n"
"   :alt: eina de moure"

#: ../../reference_manual/tools/move.rst:0
msgid ".. image:: images/tools/Movetool_coordinates.png"
msgstr ".. image:: images/tools/Movetool_coordinates.png"

#: ../../reference_manual/tools/move.rst:1
msgid "Krita's move tool reference."
msgstr "Referència de l'eina Mou del Krita."

#: ../../reference_manual/tools/move.rst:11
msgid "Tools"
msgstr "Eines"

#: ../../reference_manual/tools/move.rst:11
msgid "Move"
msgstr "Mou"

#: ../../reference_manual/tools/move.rst:11
msgid "Transform"
msgstr "Transforma"

#: ../../reference_manual/tools/move.rst:16
msgid "Move Tool"
msgstr "Eina de moure"

#: ../../reference_manual/tools/move.rst:18
msgid "|toolmove|"
msgstr "|toolmove|"

#: ../../reference_manual/tools/move.rst:20
msgid ""
"With this tool, you can move the current layer or selection by dragging the "
"mouse."
msgstr ""
"Amb aquesta eina, podreu moure la capa o selecció actual arrossegant amb el "
"ratolí."

#: ../../reference_manual/tools/move.rst:22
msgid "Move current layer"
msgstr "Mou la capa actual"

#: ../../reference_manual/tools/move.rst:23
msgid "Anything that is on the selected layer will be moved."
msgstr "Es mourà qualsevol cosa que es trobi a la capa seleccionada."

#: ../../reference_manual/tools/move.rst:24
msgid "Move layer with content"
msgstr "Mou la capa amb contingut"

#: ../../reference_manual/tools/move.rst:25
msgid ""
"Any content contained on the layer that is resting under the four-headed "
"Move cursor will be moved."
msgstr ""
"Es mourà qualsevol contingut que es trobi sobre la capa i que resti sota el "
"cursor de moviment amb quatre caps."

#: ../../reference_manual/tools/move.rst:26
msgid "Move the whole group"
msgstr "Mou el grup sencer"

#: ../../reference_manual/tools/move.rst:27
msgid ""
"All content on all layers will move.  Depending on the number of layers this "
"might result in slow and, sometimes, jerky movements. Use this option "
"sparingly or only when necessary."
msgstr ""
"Es mourà tot el contingut que es trobi sobre totes les capes. Depenent del "
"nombre de capes, això podria resultar en moviments lents i, de vegades, "
"bruscos. Utilitzeu aquesta opció amb moderació o només quan sigui necessari."

#: ../../reference_manual/tools/move.rst:28
msgid "Shortcut move distance (3.0+)"
msgstr "Drecera per a la distància a moure (3.0+)"

#: ../../reference_manual/tools/move.rst:29
msgid ""
"This allows you to set how much, and in which units, the :kbd:`Left Arrow`, :"
"kbd:`Up Arrow`, :kbd:`Right Arrow` and :kbd:`Down Arrow` cursor key actions "
"will move the layer."
msgstr ""
"Permetrà establir quant i en quines unitats les accions de les tecles del "
"cursor :kbd:`Fletxa esquerra`, :kbd:`Fletxa amunt`, :kbd:`Fletxa dreta` i :"
"kbd:`Fletxa avall` mouran la capa."

#: ../../reference_manual/tools/move.rst:30
msgid "Large Move Scale (3.0+)"
msgstr "Escala per al moviment gran (3.0+)"

#: ../../reference_manual/tools/move.rst:31
msgid ""
"Allows you to multiply the movement of the Shortcut Move Distance when "
"pressing the :kbd:`Shift` key before pressing a direction key."
msgstr ""
"Permet multiplicar el moviment de la Drecera per a la distància a moure en "
"prémer la tecla :kbd:`Majús.` abans de prémer una tecla de direcció."

#: ../../reference_manual/tools/move.rst:32
msgid "Show coordinates"
msgstr "Mostra les coordenades"

#: ../../reference_manual/tools/move.rst:33
msgid ""
"When toggled will show the coordinates of the top-left pixel of the moved "
"layer in a floating window."
msgstr ""
"Quan s'alterni, es mostraran les coordenades del píxel superior esquerre de "
"la capa moguda en una finestra flotant."

#: ../../reference_manual/tools/move.rst:35
msgid ""
"If you click, then press the :kbd:`Shift` key, then move the layer, movement "
"is constrained to the horizontal and vertical directions. If you press the :"
"kbd:`Shift` key, then click, then move, all layers will be moved, with the "
"movement constrained to the horizontal and vertical directions"
msgstr ""
"Si feu clic, premeu la tecla :kbd:`Majús.` i després moveu la capa, el "
"moviment estarà restringit a les direccions horitzontal i vertical. Si "
"premeu la tecla :kbd:`Majús.`, feu clic i després moveu, es mouran totes les "
"capes, amb el moviment restringit a les direccions horitzontal i vertical."

#: ../../reference_manual/tools/move.rst:37
msgid "Constrained movement"
msgstr "Moviment restringit"

#: ../../reference_manual/tools/move.rst:40
msgid ""
"Gives the top-left coordinate of the layer, can also be manually edited."
msgstr ""
"Dóna la coordenada superior esquerra de la capa, també es pot editar "
"manualment."
