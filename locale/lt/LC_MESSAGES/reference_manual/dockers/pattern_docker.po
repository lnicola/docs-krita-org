# Lithuanian translations for Krita Manual package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-02-19 03:36+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: lt\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""

#: ../../reference_manual/dockers/pattern_docker.rst:1
msgid "Overview of the pattern docker."
msgstr ""

#: ../../reference_manual/dockers/pattern_docker.rst:11
msgid "Patterns"
msgstr ""

#: ../../reference_manual/dockers/pattern_docker.rst:16
msgid "Patterns Docker"
msgstr ""

#: ../../reference_manual/dockers/pattern_docker.rst:19
msgid ".. image:: images/dockers/Krita_Patterns_Docker.png"
msgstr ""

#: ../../reference_manual/dockers/pattern_docker.rst:20
msgid ""
"This docker allows you to select the global pattern. Using the open-file "
"button you can import patterns. Some common shortcuts are the following:"
msgstr ""

#: ../../reference_manual/dockers/pattern_docker.rst:22
msgid "|mouseright| a swatch will allow you to set tags."
msgstr ""

#: ../../reference_manual/dockers/pattern_docker.rst:23
msgid "|mouseleft| a swatch will allow you to set it as global pattern."
msgstr ""

#: ../../reference_manual/dockers/pattern_docker.rst:24
msgid ":kbd:`Ctrl + scroll` you can resize the swatch sizes."
msgstr ""
