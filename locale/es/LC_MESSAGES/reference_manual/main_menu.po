# Spanish translations for docs_krita_org_reference_manual___main_menu.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Sofia Priego <spriego@darksylvania.net>, %Y.
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___main_menu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-19 19:27+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.2\n"

#: ../../reference_manual/main_menu.rst:5
msgid "Main Menu"
msgstr "El menú principal"

#: ../../reference_manual/main_menu.rst:7
msgid ""
"A list of all of main menu actions and a short description on what they do."
msgstr ""
"Una lista de las acciones del menú principal y una breve descripción de lo "
"que hacen."
