# Spanish translations for docs_krita_org_reference_manual___tools___polygonal_select.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
# Sofia Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___tools___polygonal_select\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-07-14 13:39+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../<generated>:1
msgid "Anti-aliasing"
msgstr "Suavizado de bordes"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../<rst_epilog>:70
msgid ""
".. image:: images/icons/polygonal_select_tool.svg\n"
"   :alt: toolselectpolygon"
msgstr ""
".. image:: images/icons/polygonal_select_tool.svg\n"
"   :alt: toolselectpolygon"

#: ../../reference_manual/tools/polygonal_select.rst:1
msgid "Krita's polygonal selection tool reference."
msgstr ""

#: ../../reference_manual/tools/polygonal_select.rst:11
msgid "Tools"
msgstr "Herramientas"

#: ../../reference_manual/tools/polygonal_select.rst:11
msgid "Polygon"
msgstr "Polígono"

#: ../../reference_manual/tools/polygonal_select.rst:11
msgid "Selection"
msgstr "Selección"

#: ../../reference_manual/tools/polygonal_select.rst:11
msgid "Polygonal Selection"
msgstr "Selección poligonal"

#: ../../reference_manual/tools/polygonal_select.rst:16
msgid "Polygonal Selection Tool"
msgstr "Herramienta de selección poligonal"

#: ../../reference_manual/tools/polygonal_select.rst:18
msgid "|toolselectpolygon|"
msgstr "|toolselectpolygon|"

#: ../../reference_manual/tools/polygonal_select.rst:20
msgid ""
"This tool, represented by a polygon with a dashed border, allows you to "
"make :ref:`selections_basics` of a polygonal area point by point. Click "
"where you want each point of the Polygon to be. Double click to end your "
"polygon and finalize your selection area."
msgstr ""

#: ../../reference_manual/tools/polygonal_select.rst:24
msgid "Hotkeys and Sticky keys"
msgstr ""

#: ../../reference_manual/tools/polygonal_select.rst:26
msgid ""
":kbd:`R` sets the selection to 'replace' in the tool options, this is the "
"default mode."
msgstr ""

#: ../../reference_manual/tools/polygonal_select.rst:27
msgid ":kbd:`A` sets the selection to 'add' in the tool options."
msgstr ""

#: ../../reference_manual/tools/polygonal_select.rst:28
msgid ":kbd:`S` sets the selection to 'subtract' in the tool options."
msgstr ""

#: ../../reference_manual/tools/polygonal_select.rst:29
msgid ""
":kbd:`Shift +` |mouseleft| sets the subsequent selection to 'add'. You can "
"release the :kbd:`Shift` key while dragging, but it will still be set to "
"'add'. Same for the others."
msgstr ""

#: ../../reference_manual/tools/polygonal_select.rst:30
msgid ":kbd:`Alt +` |mouseleft| sets the subsequent selection to 'subtract'."
msgstr ""

#: ../../reference_manual/tools/polygonal_select.rst:31
msgid ":kbd:`Ctrl +` |mouseleft| sets the subsequent selection to 'replace'."
msgstr ""

#: ../../reference_manual/tools/polygonal_select.rst:32
msgid ""
":kbd:`Shift + Alt +` |mouseleft| sets the subsequent selection to "
"'intersect'."
msgstr ""

#: ../../reference_manual/tools/polygonal_select.rst:36
msgid "Hovering over a selection allows you to move it."
msgstr ""

#: ../../reference_manual/tools/polygonal_select.rst:37
msgid ""
"|mouseright| will open up a selection quick menu with amongst others the "
"ability to edit the selection."
msgstr ""

#: ../../reference_manual/tools/polygonal_select.rst:41
msgid ""
"You can switch the behavior of the :kbd:`Alt` key to use the :kbd:`Ctrl` key "
"instead by toggling the switch in the :ref:`general_settings`."
msgstr ""

#: ../../reference_manual/tools/polygonal_select.rst:44
msgid "Tool Options"
msgstr "Opciones de la herramienta"

#: ../../reference_manual/tools/polygonal_select.rst:47
msgid ""
"This toggles whether or not to give selections feathered edges. Some people "
"prefer hard-jagged edges for their selections."
msgstr ""
